﻿namespace Webmilio.Commons.DependencyInjection
{
    public enum ServiceType
    {
        Singleton,
        Transient
    }
}